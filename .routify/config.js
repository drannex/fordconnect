module.exports = {
  "pages": "src/pages",
  "sourceDir": "public",
  "routifyDir": ".routify",
  "ignore": "",
  "dynamicImports": true,
  "singleBuild": true,
  "noHashScroll": false,
  "distDir": "dist",
  "hashScroll": true,
  "extensions": [
    "html",
    "svelte",
    "md",
    "svx"
  ],
  "started": "2021-07-16T09:35:26.827Z"
}