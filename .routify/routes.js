
/**
 * @roxi/routify 2.18.0
 * File generated Fri Jul 16 2021 04:35:26 GMT-0500 (Central Daylight Time)
 */

export const __version = "2.18.0"
export const __timestamp = "2021-07-16T09:35:26.921Z"

//buildRoutes
import { buildClientTree } from "@roxi/routify/runtime/buildRoutes"

//imports


//options
export const options = {}

//tree
export const _tree = {
  "root": true,
  "children": [
    {
      "isDir": true,
      "children": [
        {
          "isFallback": true,
          "path": "/app/_fallback",
          "id": "_app__fallback",
          "component": () => import('../src/pages/app/_fallback.svelte').then(m => m.default)
        },
        {
          "isDir": true,
          "children": [
            {
              "isPage": true,
              "path": "/app/account/automation",
              "id": "_app_account_automation",
              "component": () => import('../src/pages/app/account/automation.svelte').then(m => m.default)
            },
            {
              "isIndex": true,
              "isPage": true,
              "path": "/app/account/index",
              "id": "_app_account_index",
              "component": () => import('../src/pages/app/account/index.svelte').then(m => m.default)
            },
            {
              "isDir": true,
              "ext": "",
              "children": [
                {
                  "isPage": true,
                  "path": "/app/account/legal/credits",
                  "id": "_app_account_legal_credits",
                  "component": () => import('../src/pages/app/account/legal/credits.svelte').then(m => m.default)
                },
                {
                  "isPage": true,
                  "path": "/app/account/legal/privacy",
                  "id": "_app_account_legal_privacy",
                  "component": () => import('../src/pages/app/account/legal/privacy.svelte').then(m => m.default)
                },
                {
                  "isPage": true,
                  "path": "/app/account/legal/tos",
                  "id": "_app_account_legal_tos",
                  "component": () => import('../src/pages/app/account/legal/tos.svelte').then(m => m.default)
                }
              ],
              "path": "/app/account/legal"
            },
            {
              "isDir": true,
              "children": [
                {
                  "isPage": true,
                  "path": "/app/account/messages/:slug",
                  "id": "_app_account_messages__slug",
                  "component": () => import('../src/pages/app/account/messages/[slug].svelte').then(m => m.default)
                },
                {
                  "isIndex": true,
                  "isPage": true,
                  "path": "/app/account/messages/index",
                  "id": "_app_account_messages_index",
                  "component": () => import('../src/pages/app/account/messages/index.svelte').then(m => m.default)
                }
              ],
              "isLayout": true,
              "path": "/app/account/messages",
              "id": "_app_account_messages__layout",
              "component": () => import('../src/pages/app/account/messages/_layout.svelte').then(m => m.default)
            }
          ],
          "isLayout": true,
          "path": "/app/account",
          "id": "_app_account__layout",
          "component": () => import('../src/pages/app/account/_layout.svelte').then(m => m.default)
        },
        {
          "isIndex": true,
          "isPage": true,
          "path": "/app/index",
          "id": "_app_index",
          "component": () => import('../src/pages/app/index.svelte').then(m => m.default)
        },
        {
          "isDir": true,
          "children": [
            {
              "isPage": true,
              "path": "/app/pages/location",
              "id": "_app_pages_location",
              "component": () => import('../src/pages/app/pages/location.svelte').then(m => m.default)
            },
            {
              "isPage": true,
              "path": "/app/pages/more",
              "id": "_app_pages_more",
              "component": () => import('../src/pages/app/pages/more.svelte').then(m => m.default)
            },
            {
              "isPage": true,
              "path": "/app/pages/test",
              "id": "_app_pages_test",
              "component": () => import('../src/pages/app/pages/test.svelte').then(m => m.default)
            }
          ],
          "isLayout": true,
          "path": "/app/pages",
          "id": "_app_pages__layout",
          "component": () => import('../src/pages/app/pages/_layout.svelte').then(m => m.default)
        }
      ],
      "isLayout": true,
      "path": "/app",
      "id": "_app__layout",
      "component": () => import('../src/pages/app/_layout.svelte').then(m => m.default)
    },
    {
      "isDir": true,
      "ext": "",
      "children": [
        {
          "isPage": true,
          "path": "/auth/login",
          "id": "_auth_login",
          "component": () => import('../src/pages/auth/login.svelte').then(m => m.default)
        },
        {
          "isPage": true,
          "path": "/auth/logout",
          "id": "_auth_logout",
          "component": () => import('../src/pages/auth/logout.svelte').then(m => m.default)
        },
        {
          "isDir": true,
          "ext": "",
          "children": [
            {
              "isPage": true,
              "path": "/auth/setup/loading",
              "id": "_auth_setup_loading",
              "component": () => import('../src/pages/auth/setup/loading.svelte').then(m => m.default)
            },
            {
              "isPage": true,
              "path": "/auth/setup/terms",
              "id": "_auth_setup_terms",
              "component": () => import('../src/pages/auth/setup/terms.svelte').then(m => m.default)
            }
          ],
          "path": "/auth/setup"
        },
        {
          "isPage": true,
          "path": "/auth/setup",
          "id": "_auth_setup",
          "component": () => import('../src/pages/auth/setup.svelte').then(m => m.default)
        }
      ],
      "path": "/auth"
    },
    {
      "isIndex": true,
      "isPage": true,
      "path": "/index",
      "id": "_index",
      "component": () => import('../src/pages/index.svelte').then(m => m.default)
    }
  ],
  "isLayout": true,
  "path": "/",
  "id": "__layout",
  "component": () => import('../src/pages/_layout.svelte').then(m => m.default)
}


export const {tree, routes} = buildClientTree(_tree)

