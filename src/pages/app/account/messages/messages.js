export let messages = [
    {
        title: "Welcome",
        content: "Messages are not currently supported by the API, but I will display what they would look like here."
    }, {
        title: "This is the second message",
        content: "These are just a few messages that would show up, you can click on them and view the whole message if you wanted."
    }, {
        title: "This is the third message",
        content: "These are just a few messages that would show up, you can click on them and view the whole message if you wanted."
    }, {
        title: "None of these are hardcoded when clicked",
        content: "They all pull from one central location for the inbox/message center and the individual message pages"
    }
]