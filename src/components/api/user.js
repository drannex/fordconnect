import {getStorageToken, setStorageToken, resetStorage} from './storage';
import ky from 'ky';

// Login -> Get ID_Code -> Get Token Data (Once!) -> after every twenty minutes (15 for a nice buffer) get a refreshed token using the previous refreshed data.

// Init, first thing to do after logging in 
// This also works as a reset function. 
export async function init(code) {

    if (code.length < 10) {
        const msg = "[Init] :: Initializing or resetting requires an access_code or a string longer than ten characters as a safety feature."
        console.log(msg)
        return msg;
    } else {
        reset();
        console.log("[Init] :: Rebuilding Application Storage")
        var ApplicationInfo = {
            'api_version': '2020-06-01',
            'grant_type': 'authorization_code',
            'application_id': 'afdc085b-377a-4351-b23e-5e1d35fb3700',
            'client_id': '30990062-9618-40e1-a27b-7c6bcb23658a',
            'client_secret': 'T_Wk41dx2U9v22R5sQD4Z_E1u-l2B-jXHE',
            'redirect_uri': 'https://localhost:3000',
        };

        var TokenDummy = {
            "access_token":"none",
            "id_token":"",
            "token_type":"Bearer",
            "not_before":1626337430,
            "expires_in":1200,
            "expires_on":1626338630,
        }

        const {setApplicationInfo} = await setStorageToken('Application', JSON.stringify(ApplicationInfo));
        const {setFirstLoad} = await setStorageToken('init', "true");
        
        const createCode = await getToken(code); // Get the Initial Token Data on Application Setup/login

        return setApplicationInfo;
    }
}

export async function reset() {
    console.log("[Init] :: Resetting Storage")
    const reset = await resetStorage();
    return reset;
}

export async function getApplication() {
    var Application = await getStorageToken('Application');
    return Application;
}

export async function getAccessToken() {
    var token = JSON.parse(await getStorageToken('Token'));
    var currentTime = Math.round(Date.now() / 1000);

    if (token.expires_on === null) {
        return getAccessToken();
    } else {
        if (token.expires_on) {
            // Check if the token has expired (with a healthy buffer)
            if (token.expires_on > currentTime + 300 ) { // A five minute buffer area.
                console.log("[Token] :: Token acceptable.")
                var access_token = token.access_token;
                return access_token;
            } else { // If token has expired, call for a refresh token. Need to handle errors. 
                console.log("[Token] :: Refresh token required.")
                var refreshToken = await getRefreshToken();

                var token_updated = JSON.parse(await getStorageToken('Token'));
                var access_token = token_updated.access_token;

                return access_token;
            }
        }
        else {
            return getAccessToken();
        }
}
}

export async function getToken(code) {
    var Application = JSON.parse(await getStorageToken('Application'));

    const searchParams = new URLSearchParams(); 
        searchParams.set('grant_type', 'authorization_code');
        searchParams.set('client_id', Application.client_id);
        searchParams.set('client_secret', Application.client_secret);
        searchParams.set('code', code);
        searchParams.set('redirect_uri', Application.redirect_uri);

    const TokenReq = await 
        ky.post('https://dah2vb2cprod.b2clogin.com/914d88b1-3523-4bf6-9be4-1b96b4f6f919/oauth2/v2.0/token?p=B2C_1A_signup_signin_common', 
        {body: searchParams}).json();
    var TokenData = await setStorageToken('Token', JSON.stringify(TokenReq))

    var refresh = getRefreshToken();

    return TokenData;
}

export async function getRefreshToken() {
    var Application = JSON.parse(await getStorageToken('Application'));
    var TokenStorage = JSON.parse(await getStorageToken('Token'));

    var formData = new FormData(); 
        formData.append('grant_type', 'refresh_token');
        formData.append('refresh_token', TokenStorage.refresh_token);
        formData.append('client_id', Application.client_id);
        formData.append('client_secret', Application.client_secret);

    const TokenReq = await 
        ky.post('https://dah2vb2cprod.b2clogin.com/914d88b1-3523-4bf6-9be4-1b96b4f6f919/oauth2/v2.0/token?p=B2C_1A_signup_signin_common', 
        { body: formData }).json();

    var TokenData = await setStorageToken('Token', JSON.stringify(TokenReq))
    return TokenData;
}

// This quickly checks access, should be called fairly often but used mostly for login and app view requests.
export async function checkAccess() {
    var Token = JSON.parse(await getStorageToken('Token'));

    console.log(Token);

    if (!Token) {
        return false;
    } else {
        return true;
    }
}