import { Storage } from '@capacitor/storage';

// Core Storage Functions 
export async function setStorageToken(key, value) {
  await Storage.set({
    key: key,
    value: value,
  });
  console.log("[Storage] :: ", key, 'saved.')
  return {key, value}
};

export async function getStorageToken(key) {
  const value = await Storage.get({ key: key });
  console.log("[Storage] :: ", key, 'read.')
  return value.value;
};

export async function resetStorage() {
  console.log("[Storage] :: All storage has been cleared.")
  Storage.clear();
}